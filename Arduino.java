/**
*
* Arduino class
*
* a class used to represent arduino board
*
* also used as example for compareTo method
*
* ----- compile -----
*
* javac Main.java
*
* ----- run -----
*
* java Main
*
*/
class Arduino{
    public String model;
    public Arduino(String model){
        this.model=model;
    }
    public int compareTo(Object obj){ // override compareTo method
        Arduino ardu=(Arduino)obj;
        return this.model.compareTo(ardu.model); // compare arduino models, return 0 if equal
    }
}
