Discussion 4
------------

- Rpi.java (example override equals method, check comments for details)
- Arduino.java (example override compareTo method)
- Main.java (run example)

Test environment
----------------

- os lubuntu 16.04 kernel version 4.13.0
- javac version 11.0.5
