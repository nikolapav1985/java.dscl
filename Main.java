public class Main{
    public static void main(String args[]){
        Rpi ra,rb,rc;
        Arduino arda,ardb,ardc;
        ra=new Rpi("rpimodel2");
        rb=new Rpi("rpimodel2");
        rc=new Rpi("rpimodel3");
        arda=new Arduino("arduno");
        ardb=new Arduino("arduno");
        ardc=new Arduino("ardnano");
        System.out.println(ra.equals(rb)); // these are equal
        System.out.println(ra.equals(rc)); // these are not equal
        System.out.println(arda.compareTo(ardb)); // these are equal, should be zero
        System.out.println(arda.compareTo(ardc)); // these are not equal, should not be zero
    }
}
