/**
*
* Rpi class
*
* a class used to represent raspberry pi board
*
* also used as example for equals method
*
* ----- advantages -----
*
* There is more CONTROL once equals is overriden (comparison can happen on any property). Also CAN be used to compare 
* custom objects (in addition to already existing ones).
*
* ----- disadvantages -----
*
* ERRORS are possible if method is overriden (if no prior testing and debugging done). Also programmer needs to ensure 
* RST (reflexive, symmetric and transitive) property of equality relation.
*
* ----- compile -----
*
* javac Main.java
*
* ----- run -----
*
* java Main
*
*/
class Rpi{
    public String model;
    public Rpi(String model){
        this.model=model;
    }
    public boolean equals(Object obj){ // override equals method
        Rpi rpi=(Rpi)obj;
        return this.model.equals(rpi.model); // compare rpi models
    }
}
